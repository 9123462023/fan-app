import React, { useEffect, useState } from 'react';
import {Dimensions , View, Image, StyleSheet, Text, TouchableOpacity , StatusBar  } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { responsiveHeight, responsiveWidth, responsiveFontSize} from "react-native-responsive-dimensions";

const App = () => {


  return (
    <View style={styles.container}>
            
        
         {/* status bar color change */}
           
            <StatusBar  hidden = {false} backgroundColor = "#111111" />


      <View style={styles.view_top}>

          <View style={styles.view_nav}>

          <Icon name="arrow-left" size={20} color="green" />

            <Text style={styles.text_top}> Send Money Confirmation</Text>
          <Icon name="arrow-right" size={30} color="transparent" />


          </View>

          <Image source={require('./avatar.jpeg')} style={styles.view_image} />

          <View>

            <Text style={styles.view_title}> Demo User 2</Text>
            <Text style={styles.view_text}> From illicocash CDF Wallet</Text>

          </View>
      </View>
      


      <View style={ styles.borderStyles}/>

        <View style={styles.view_bottom}>

 

          <Image source={require('./avatar.jpeg')} style={styles.image_bottom} />

            <View style={styles.view_bottom_text}>

              <Text style={styles.text1}> Test User </Text>
              <Text style={styles.text2}> To illicoash account </Text>
              <Text style={styles.text3}> send money amount: CDF 10.0</Text>

            </View>

            <TouchableOpacity style={styles.bottom_button}>
             <Text style={styles.text_button}> Confirm Transaction</Text>

            </TouchableOpacity>
          
        </View>

     

    </View>
  );
};
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    backgroundColor:'#444444'
  },

  view_top:{
    height:responsiveHeight(40) ,
   justifyContent:'space-between' ,
    alignItems:'center'} ,

  view_nav: {
    justifyContent:'space-between' ,
   alignItems:'center' , 
   flexDirection:'row' , 
   width:responsiveWidth(95) , 
   marginTop:responsiveHeight(2),

   alignSelf:'center' },

  view_image:{
    width:responsiveWidth(25) ,
     height:responsiveWidth(25) ,
      borderRadius:responsiveWidth(25) ,
       borderWidth:1 , 
       borderColor:'transparent' , 
       alignSelf:'center'},

  view_title:{
    fontWeight:'bold' ,
   fontSize:responsiveFontSize(2.5) ,
    color:'orange' ,
     textAlign:'center'} ,

  view_text:{
    color:'white' ,
     textAlign:'center' ,
      marginTop:responsiveHeight(3)},

  view_bottom:{
    backgroundColor:'#252525',
   height:responsiveHeight(55) , 
   marginTop:-responsiveHeight(10) , 
   justifyContent:'space-around' } ,

  image_bottom:{  marginTop:responsiveHeight(10) , 
    width:responsiveWidth(25) ,
     height:responsiveWidth(25) ,
      borderRadius:responsiveWidth(12.5)  , 
      borderWidth:1 ,
       borderColor:'transparent' , 
       alignSelf:'center'},

  view_bottom_text:{
    justifyContent:'center' , 
    alignItems:'center'
},

  text1:{
    fontWeight:'bold' ,
     fontSize:responsiveFontSize(2.5) ,
      color:'orange' ,
       textAlign:'center'
      },
  text2:{
    fontSize:responsiveFontSize(2) ,
     color:'white' ,
      textAlign:'center' , 
      marginVertical:responsiveHeight(2)} ,

  text3:{color:'orange' ,
   fontSize:responsiveFontSize(2.25) ,
    textAlign:'center'},

  bottom_button:{
    borderRadius:10 , 
     height:responsiveHeight(7.5)  ,
      width:responsiveWidth(95) ,
       alignSelf:'center' , 
       justifyContent:'center' , 
       alignItems:'center' , 
       backgroundColor:'orange'},

  text_button:{fontWeight:'bold' ,
   fontSize:responsiveFontSize(2.5) ,
    color:'white' , 
    textAlign:'center'},

  borderStyles:{
         zIndex:1,
         borderTopWidth: responsiveHeight(10),
         borderRightWidth: responsiveWidth(50),
         borderBottomWidth: 0,
         borderLeftWidth: responsiveWidth(50),
         borderTopColor: '#444444',
         borderRightColor: 'transparent',
         borderBottomColor: 'transparent',
         borderLeftColor: 'transparent',
         width: 0,
         height: 0,
         backgroundColor: 'transparent',
         borderStyle: 'solid',
         alignSelf:'center'
  },
  text_top:{
    fontWeight:'bold' ,
     fontSize:16 ,
      color:'white',
      textAlign:'center'
    }
});

export default App;
